﻿using System;

namespace ex_26
{
    class Program
    {
        static void Main(string[] args)
        {
            var student = new Student();
            student.Name = "Sing";
            student.ID = "10013041";
            student.PrintStudentInfo();
        }
    }

    // a)
    // Create a Class and call it Student
    // Set Fields for Name, Id number
    // Create properties that allow you to change the values - the Fields are not allowed to be accessed directly.
    // Create a method that prints out the name and id number using Properties.
    class Student
    {
        private String StudentName;
        private string StudentID;

        public String Name
        {
            set
            {
                StudentName = value;
            }
            get
            {
                return Name;
            }
        }

        public String ID
        {
            set
            {
                StudentID = value;
            }
            get
            {
                return Name;
            }
        }


        public void PrintStudentInfo()
        {
            Console.WriteLine("Student Information\n Name : {0} \n ID : {1}", StudentName, StudentID);
        }
    }
}
